# The Unofficial Arch Linux Instalation Guide

https://archguide.vercel.app/index.html

This guide was created from personal instructions aiming at filling the blanks in the official Arch Linux Installation Guide. Slowly it evolved into a complete step-by-step guide which is now hosted on the http and in geminispace.

This guide is not a replacement for the official Installation Guide, may not be up to date and contain some questionable decisions.

